import {HotkeyBinder} from './HotkeyBinder'

export {HotkeyModel, HotkeyModelMods} from './models/HotkeyModel'
export const HotkeyManager = new HotkeyBinder()
