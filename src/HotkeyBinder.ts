import { HotkeyModel, HotkeyModelMods } from './models/HotkeyModel'

interface DOMFunctions {
	[key: string]: HotkeyEventListener
}

type BatchHotkey = HotkeyModel[]

type BatchHotkeyName = string[]

interface HotkeyEventListener {
	hotkeyModel: HotkeyModel[]
	fn: Function[]
	eventListener: EventListenerOrEventListenerObject
}

export class HotkeyBinder {
	private hotkeys: DOMFunctions
  private canTrigger = true;

	constructor() {
		this.hotkeys = {}
	}

	public addBatchHotkey = (batchHotkey: BatchHotkey): BatchHotkeyName => {
		const batchName = []
		for (const hotkey of batchHotkey) {
			batchName.push(this.addHotkey(hotkey))
		}
		return batchName
	}

	public addHotkeyMod = (hotkey: HotkeyModelMods): string => {
		const mod = (hotkey.shift ? 'shift' : '').concat(hotkey.alt ? 'alt' : '').concat(hotkey.ctrl ? 'ctrl' : '');
		const bindedKey = {...hotkey, modifier: mod}
		delete bindedKey.ctrl
		delete bindedKey.shift
		delete bindedKey.alt

		return this.addHotkey(bindedKey as HotkeyModel)
	}

	public addHotkey = (hotkey: HotkeyModel): string => {
		if (!hotkey) {
			throw new Error("No HotkeyModel was given !")
		}
		const cpyHotkey = {...hotkey}
		if (!cpyHotkey.specificId) {
			cpyHotkey.specificId = this.getHotkeySpecificName(cpyHotkey)
		}

		this.appendToElement(cpyHotkey)

		return cpyHotkey.specificId
	}

	public removeBatchHotkey = (batchHotkey: BatchHotkey): void => {
		for (const hotkey of batchHotkey) {
			this.removeHotkeyFromModel(hotkey)
		}
	}

	public removeHotkeyFromModel = (hotkey: HotkeyModel): void => {
		if (!hotkey) {
			throw new Error("No HotkeyModel was given to remove from DOM !")
		}
		let bindedName = hotkey.specificId
		if (!bindedName) {
			bindedName = this.getHotkeySpecificName(hotkey)
		}
		const elementRef = this.getHotkeyElementRef(hotkey)
		this.deleteHotkey([...this.hotkeys[elementRef].hotkeyModel], bindedName, elementRef)
		// if (this.hotkeys[elementRef].hotkeyModel.length <= 0) {
		// 	this.removeEventFromDOM(hotkey.element, this.hotkeys[elementRef].eventListener, hotkey.eventName)
		// }
	}

	public removeHotkeyFromId = (elementRef: string, specificId: string): void => {
		if (!specificId) {
			throw new Error("No specificId was given to remove from DOM !")
		}
		if (!elementRef) {
			elementRef = 'document'
		}
		this.deleteHotkey([...this.hotkeys[elementRef].hotkeyModel], specificId, elementRef)
	}

	public cleanHotkeys = (): void => {
		if (!this.hotkeys) {
			this.hotkeys = {}
			return
		}

		for (const fns in this.hotkeys) {
			if (!fns) {
				continue
			}
			for (const hotkey of this.hotkeys[fns].hotkeyModel) {
				this.deleteHotkey([...this.hotkeys[fns].hotkeyModel], hotkey.specificId, fns)
				// if (this.hotkeys[fns].hotkeyModel.length <= 0) {
				// 	this.removeEventFromDOM(this.hotkeys[fns].hotkeyModel[0].element, this.hotkeys[fns].eventListener, hotkey.eventName)
				// }
			}
		}
		this.hotkeys = {}
	}

	public getHotkeySpecificName = (hotkey: HotkeyModel): string => {
		return btoa(JSON.stringify(hotkey)).slice(0, 20)
	}

	public getHotkeyElementRef = (hotkey: HotkeyModel): string => {
		const {nodeName} = hotkey.element
		let event = hotkey.eventName
		if (!event) {
			event = 'keyup'
		}
		// @ts-ignore
		const attached = nodeName.toLowerCase() === 'document' ? '' : hotkey.element.id + ''
		return `${nodeName}_${attached}_${event}`
	}

	private deleteHotkey = (hotkeyToRemove: BatchHotkey, bindedName: string, elementRef: string) => {
		if (!hotkeyToRemove) {
			throw new Error(`No HotkeyModel found to remove, it's not recommend to change it after it was initialize !`)
		}
		const id = hotkeyToRemove.indexOf(hotkeyToRemove.find(val => val.specificId === bindedName))
		this.hotkeys[elementRef].hotkeyModel = hotkeyToRemove.filter(val => val.specificId !== bindedName)
		this.hotkeys[elementRef].fn = this.hotkeys[elementRef].fn.filter(fn => fn !== this.hotkeys[elementRef].fn[id])
	}

	// private removeEventFromDOM = (element: Element | Document, fn: EventListenerOrEventListenerObject, eventName: string) => {
	// 	element.removeEventListener(eventName, fn)
	// }

	private appendToElement = (cpyHotkey: HotkeyModel) => {
		let eventFunction: any
		if (!cpyHotkey.eventName) {
			cpyHotkey.eventName = "keyup"
		}
		if (!cpyHotkey.element) {
			cpyHotkey.element = document
		}
		if (!cpyHotkey.fn) {
			cpyHotkey.fn = () => {
				console.warn(`You should really add a function to this key \"${cpyHotkey.key}\" on \"${cpyHotkey.eventName}"`)
			}
		}
		if (!cpyHotkey.customShouldTrigger) {
			cpyHotkey.customShouldTrigger = () => true
		}
		const elementRef = this.getHotkeyElementRef(cpyHotkey)
		if (!this.hotkeys[elementRef]) {
			this.hotkeys[elementRef] = {
				eventListener: (event) => {
					for (const ele of this.hotkeys[elementRef].fn) {
						ele(event)
					}
				},
				hotkeyModel: [],
				fn: []
			}
			cpyHotkey.element.addEventListener(cpyHotkey.eventName, this.hotkeys[elementRef].eventListener, {
				capture: cpyHotkey.capture, passive: false, once: false
			})
		}
		eventFunction = (event: KeyboardEvent) => {
				if (this.shouldTriggerEvent(event, cpyHotkey)) {
					if (!!cpyHotkey.delay) {
						this.canTrigger = false
						setTimeout(() => this.canTrigger = true, cpyHotkey.delay)
					}
					cpyHotkey.fn()
					if (cpyHotkey.preventBubbleUp) {
						event.stopPropagation()
						event.stopImmediatePropagation()
					}
					if (cpyHotkey.preventDefault) {
						event.preventDefault()
					}
			}
		}
		this.hotkeys[elementRef].fn.push(eventFunction)
		this.hotkeys[elementRef].hotkeyModel.push(cpyHotkey)
	}

	private shouldTriggerEvent = (event: KeyboardEvent, hotkey: HotkeyModel): boolean => {
		const {modifier} = hotkey

		return !event.defaultPrevented && hotkey.customShouldTrigger() && this.canTrigger &&
      modifier.includes("shift") === event.shiftKey &&
      modifier.includes("ctrl") === event.ctrlKey &&
			modifier.includes("alt") === event.altKey &&
      this.checkKeys(event.key , hotkey.key)
	}

	private checkKeys = (eventKey: string, definedKey: string[]): boolean => {
		return !!definedKey.find(key => key.toLowerCase() === eventKey.toLowerCase())
	}
}
