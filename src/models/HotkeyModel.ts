
interface Hotkey {
	key: string[]
	eventName?: "keyup" | "keydown" | "keypress"
	specificId?: string
	element?: Element | Document
	delay?: number
	fn?: Function
	capture?: boolean
	preventDefault?: boolean
	preventBubbleUp?: boolean
	customShouldTrigger?: () => boolean
	[key: string]: any
}

export interface HotkeyModel extends Hotkey {
	modifier?: "alt" | "ctrl" | "shift" | null
}

export interface HotkeyModelMods extends Hotkey {
	shift: boolean
	ctrl: boolean
	alt: boolean
}
